const tabsWrapper = document.querySelector('.tabs');
const tabsCollection = document.querySelector('.tabs').children;
const tabsContentCollection = document.querySelector('.tabs-content').children;

function rechooseElemToShow (elemColection, counterNum) {
    for (let i = 0; i < elemColection.length; i++) {
        if (elemColection[i].classList.contains('active')) {
            elemColection[i].classList.remove('active');
        }
        if (elemColection[i].dataset.counter === counterNum) {
            elemColection[i].classList.add('active');
        }
    }
}

function findTarget (event) {
    let targetCounter = event.target.dataset.counter;

    rechooseElemToShow(tabsCollection, targetCounter);
    rechooseElemToShow(tabsContentCollection, targetCounter);
}

tabsWrapper.addEventListener('click', findTarget);