        /*      Технический вопрос: 
    1.  setTimeout(functionName, 500) - вызывает функцию (functionName) единажды через указанное время (0,5 с, или любое другое указанное время)
        setInterval(functionName, 3000) - вызывает одну и ту же функцию (functionName) бесконечно, через указанные промежутки времени (3 с, или любой другой указанный интервал);
    2.  Если в функцию setTimeout() передать нулевую задержку, она сработает как только загрузится вся страница, и будут выполнены все остальные действия. Функция setTimeout() является асинхронной функцией, и она отработает в последнюю очередь. 
    3.  setInterval() будет повтрять действие функции бесконечно, если его не остановить функцией clearInterval() */

const imgsOfGame = document.querySelectorAll('.img-of-game');
const secondsElem = document.querySelector('.second');
const millisecondsElem = document.querySelector('.millisecond');
const stopBtn = document.querySelector('.stop-btn');
const startBtn = document.querySelector('.start-btn');

let currentImg = 0;
let secStart = 2;
let millisecStart = 100;    // хотя в 1 секунде 1000 миллисекунд, они не успевают отображаться, по-этому вывожу десятками
let interval;
let secondsCounterInterval;
let millisecondsCounterInterval;

imgsOfGame[currentImg].classList.remove('hidden-img');
secondsElem.innerHTML = `0${secStart}`;
millisecondsElem.innerHTML = millisecStart;

const startSecondCounter = function () {
    secStart--;
    secStart < 0 ? secStart = 2 : secStart;
    secondsElem.innerHTML = `0${secStart}`;
}

const startMillisecondCounter = function () {
    millisecStart--;
    millisecStart === 0 ? millisecStart = 100 : millisecStart;
    millisecondsElem.innerHTML = millisecStart;
}

const showNextImg = function () {
    currentImg++;
    let previousImg = currentImg - 1;
    previousImg < 0 ? previousImg = imgsOfGame.length - 1 : previousImg;

    currentImg === imgsOfGame.length ? currentImg = 0 : currentImg;

    imgsOfGame[previousImg].classList.add('fade')
    
    setTimeout(function () {
        imgsOfGame[previousImg].classList.add('hidden-img');
        imgsOfGame[currentImg].classList.remove('hidden-img');
        imgsOfGame[currentImg].classList.add('fade');
    }, 500)
    
    setTimeout(function () {
        imgsOfGame[previousImg].classList.remove('fade')
        imgsOfGame[currentImg].classList.remove('fade');
    }, 550)

}

interval = setInterval(showNextImg, 3000);
secondsCounterInterval = setInterval(startSecondCounter, 1000);
millisecondsCounterInterval = setInterval(startMillisecondCounter, 10);

const continueSlideShow = function () {
    secStart = 2;
    secondsElem.innerHTML = `0${secStart}`;

    millisecStart = 100;
    millisecondsElem.innerHTML = millisecStart;

    interval = setInterval(showNextImg, 3000);
    secondsCounterInterval = setInterval(startSecondCounter, 1000);
    millisecondsCounterInterval = setInterval(startMillisecondCounter, 10);

    stopBtn.disabled = false;
    startBtn.disabled = true;
}

const stopSlideShow = function () {
    clearInterval(interval);
    clearInterval(secondsCounterInterval);
    clearInterval(millisecondsCounterInterval);
    
    stopBtn.disabled = true;
    startBtn.disabled = false;
}

stopBtn.addEventListener('click', stopSlideShow);
startBtn.addEventListener('click', continueSlideShow);