/*          Технический вопрос:
DOM - Document Object Model - програмный интерфейс позволяющий получать доступ к содержимому HTML-документов, менять их структуру и содержимоею. Можно представить в виде дерева с иерархией дочерних, родительских и соседских елементов. */


                                        /* M A I N    T A S K */

let array = ["Kharkiv", "Kiev", "Borispol", "Irpin", "Odessa", "Lviv", "Dnieper"];

function arrayConvertor (array, parent = document.body) {
    let result = `<ul>${array.map(eachElem => `<li>${eachElem}</li>`).join('')}</ul>`;
    parent === document.body ? parent.insertAdjacentHTML('afterbegin', result) : document.body.insertAdjacentHTML('afterbegin', `<${parent}>${result}</${parent}>`)
}
arrayConvertor(array, 'div')


                                        /* O P T I O N A L    T A S K */

let mainArray = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper", ["hello", "world", ["Kiev", "Kharkiv"]], "Odessa", "Lviv"];

function arrayChecking (arrayForChackng) {
    let newArray = `<ul>${arrayForChackng.map(eachElem => Array.isArray(eachElem) ? `<li>${arrayChecking(eachElem)}</li>` : `<li>${eachElem}</li>`).join('')}</ul>`
    return newArray
}

function listCreator (parent = document.body) {
    let checkedArray = arrayChecking(mainArray)
    parent === document.body ? parent.insertAdjacentHTML('afterbegin', checkedArray) : document.body.insertAdjacentHTML('afterbegin', `<${parent}>${checkedArray}</${parent}>`)
}
listCreator('div')