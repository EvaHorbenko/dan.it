$('.show-more-btn').click(function () {
    $(this).html(function (i, text) {
        return text === 'show more' ? 'hide all' : 'show more'
    })
    $('.defolt-hidden').slideToggle('slow').css({'display': 'grid'})
})

$(window).scroll(function () {
    if ($(this).scrollTop() >= $(this).innerHeight()) {
        $('.scroll_to_top-btn').fadeIn()
    } else {
        $('.scroll_to_top-btn').fadeOut()
    }
})

$('.scroll_to_top-btn').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 400)
})