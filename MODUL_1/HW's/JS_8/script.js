/*          Технический вопрос
    обработчик событий - функция, которая выполнится только после определенного события */

const priceWrapper = document.querySelector('#priceWrapper');
const priceInput = document.querySelector('#priceInput');
const alertMessage = document.querySelector('#alert-message');

function checkValue () {
    if (!isNaN(priceInput.value) || priceInput.value > 0) {
        alertMessage.classList.remove('alert-message-shown');
        priceInput.classList.remove('red-text_highlight');
        if (priceInput.value !== '') {
            priceInput.classList.add('green-text');
            createElement();
        }
    } else {
        alertMessage.classList.add('alert-message-shown');
        priceInput.classList.add('red-text_highlight');
    } 
}

function createElement () {
    let priceElem = document.createElement('div');
    priceElem.className = 'current-price';
    priceElem.innerHTML = `<span>Текущая цена: ${priceInput.value}</span><button class="close-btn">X</button>`;
    priceWrapper.prepend(priceElem);
    
    priceElem.addEventListener('click', deleteElem);
}

function deleteElem (event) {
    let target = event.target;
    if (target.className === 'close-btn') {
        target.closest('.current-price').remove();
    }
    priceInput.value = '';
}

function cleanInput () {
    priceInput.classList.remove('green-text');
    priceInput.classList.remove('red-text_highlight');
}

function focusOnPriceInput () {
    priceInput.classList.add('input-focus');
    cleanInput()
    priceInput.value = '';
}

function blurOnPriceInput () {
    priceInput.classList.remove('input-focus');
    cleanInput()
    checkValue();
}

priceInput.addEventListener('focus', focusOnPriceInput)
priceInput.addEventListener('blur', blurOnPriceInput)