/*             Технический вопрос: 
рекурсия - математическая функция "вложенная" сама в себя - в процессе отработки обращается сама к себе. */

function getNum (number) {
    let operand;
    number = prompt('Enter number', number);
    operand = Number(number);
    checkNum(number, operand);
}

function checkNum (number, operand) {
    operand = Number.isNaN(operand) || operand < 1 ? getNum(number) : countFactorialRecursion(operand), showResult(operand, number);
}

/* function countFactorialRecursion (operand) {
    if (operand > 1) {
        return operand * countFactorialRecursion(operand - 1);
    } else {
        return operand;
    }
} */

function countFactorialRecursion (operand) {
    return (operand > 1) ? operand * countFactorialRecursion(operand - 1) : operand;
}

function showResult (result, number) {
    if (Number(number)) {
        console.log(`${number}! = ${result}`);
    }
}

getNum();