/*      Тухнический вопрос: 
    экранирование не дает програме считывать значения прямо, а дает им другие функции
    например \n - в тектсте выведется как перенос строки
    так же можно после обратного слеша писать юникод какого-то символа. Без обратного слеша в текстовом поле выведется сам юникод, а с обратным слешом - символ который он обозначает */

function createNewUser () {
    let newUser = {
        firstName: prompt("Enter your first name"),
        lastName: prompt("Enter your last name"),
        birthday: prompt("Enter your birthday date", "dd.mm.yyyy"),

        getLogin () {
            let login = (this.firstName.slice(0, 1) + this.lastName).toLocaleLowerCase();
            return login
        },

        convertDate () {
            let birthYear = this.birthday.slice(6);
            let birthMonth = this.birthday.slice(3, 5);
            let birthDay = this.birthday.slice(0, 2);
            let birthdayFormat = `${birthYear}-${birthMonth}-${birthDay}`
            return birthdayFormat
        },

        getAge () {
            let currentDate = new Date();
            let userBirthday = this.convertDate();
            let userBirthMileseconds = new Date(userBirthday);
            let userAgeMileseconds = currentDate.getTime() - userBirthMileseconds.getTime();

            const ONE_YEAR = 1000 * 60 * 60 * 24 * 365;
            let userAge = parseInt(userAgeMileseconds / ONE_YEAR);
            return userAge
        },

        getPassword () {
            let password = this.firstName.slice(0, 1).toUpperCase() + this.lastName.toLocaleLowerCase() + this.birthday.slice(6);
            return password
        }
    }
    return newUser
}

let createdUser = createNewUser()

let userLogin = createdUser.getLogin()
let userAge = createdUser.getAge()
let userPasword = createdUser.getPassword()

console.log(createdUser)
console.log(`Your login is ${userLogin}, \nYour age is ${userAge} years old, \nYour password is ${userPasword}`)



