/*             Технический вопрос: 
циклы дают возможность выполнять повторяющиеся инструкции, оптимизируют написание кода */

let number;

do {
    number = Number(prompt('Enter number'));
} while (number !== parseInt(number) || Number.isNaN(number) == true)

if (number >= 5) {
    for (let i = 0; i <= number; i++) {
        if (i % 5 !== 0) continue
        console.log(i)
    }
} else {
    console.log('Sorry, no numbers')
}
