/*             Технический вопрос: 
функции дают возможность выполнять одинаковые инструкции для разных переменных, оптимизируют написание кода;
при передаче аргумента функции можно управлять входящими значениями, которые будут влиять на результат работы функции */


function getNumber (iteration) {
    let operand;
    let number;
    do {
        operand = prompt(`Enter ${iteration} number`, operand);
        number = Number(operand);
    } while (Number.isNaN(number))
    return number;
}

function getSign () {
    let sign;
    do {
        sign = prompt('Enter sign', sign);
    } while (sign != '+' && sign != '-' && sign != '*' && sign != '/')
    return sign
}

function countResult (firstNumber, secondNumber, mathSign) {
    if (mathSign == '+') {
        return (firstNumber + secondNumber);
    } else if (mathSign == '-') {
        return (firstNumber - secondNumber);
    } else if (mathSign == '*') {
        return (firstNumber * secondNumber);
    } else {
        return (firstNumber / secondNumber);
    }
}

let firstNumber = getNumber('first');
let secondNumber = getNumber('second');
let mathSign = getSign();

console.log(countResult(firstNumber, secondNumber, mathSign))