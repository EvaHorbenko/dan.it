let 
    businessEmail, 
    personalEmail, 
    isBusinessEmailValid, 
    isPersonalEmailValid;

function collectEmails() {
    businessEmail = prompt("Enter your business e-mail");
    personalEmail = prompt("Enter your personal e-mail");
    validateEmails()
}

function validateEmail (email) {
    if (email.indexOf('@') < 1) {
        return false;
    } 

    if (email.indexOf('.') < email.indexOf('@')) {
        return false;
    }

    return true;
}

function validateEmails () {
    isBusinessEmailValid = validateEmail(businessEmail);
    isPersonalEmailValid = validateEmail(personalEmail);
    console.log('Business: ', isBusinessEmailValid)
    console.log('Personal: ', isPersonalEmailValid)
    if (!isPersonalEmailValid || !isBusinessEmailValid) {
        collectEmails();
    } else {
        informUser();
    }
}

function informUser() {
    if (isBusinessEmailValid && isPersonalEmailValid) {
        alert('Thank You, you have registered');
    } else {
        alert('Sorry, one of your emails is invalid');
    }
}

collectEmails();
