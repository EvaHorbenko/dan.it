function getOperand (iteration) {
  let input = Number(prompt('Enter number'));
  if (Number.isNaN(input) == true) {
    alert('Enter number')
    input = Number(prompt(`Enter ${iteration} operand one more time`));
  }
  return input;
}

let operand1 = getOperand(`first`);

let sign = prompt("Enter operator");
sign = sign === '' ? '+' : sign;
if (sign != "+" && sign != "-" && sign != "*" && sign != "/") {
  sign = prompt("Enter sign one more time")
}

let operand2 = getOperand(`second`);

let result;
if (sign == "+" ) {
  result = operand1 + operand2;
} else if (sign == "-") {
  result = operand1 - operand2;
} else if (sign == "/") {
  result = operand1 / operand2;
} else if (sign == "*") {
  result = operand1 * operand2;
} else {
  alert('Enter one of next signs: "+" "-" "*" "/"')
}

alert(result);

switch (sign) {
  case '+':
    result = operand1 + operand2;
    break;
  case '-':
    result = operand1 - operand2;
    break;
  case '/':
    result = operand1 / operand2;
    break;
  case '*':
    result = operand1 * operand2;
    break;
  default:
    alert('Enter one of next signs: "+" "-" "*" "/"');
}
alert(result);