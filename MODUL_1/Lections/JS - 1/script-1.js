const firstName = prompt('What is your name?');     // поле ввода
const age = prompt('What is your age?');
const isReady = confirm('Are you ready?');          // бинарный ответ - да/нет

/* if (isReady == false) {
    alert('You are Muggle!')
} */

const isAngry = confirm('Are you angry?');
const isKind = confirm('Are you kind?');
let isSly;

if (isAngry && isKind) {
    isSly = true;
} else {
    isSly = false;
}


let faculty;

if (isSly) {
    faculty = 'Ravenklow';
} else if (isAngry) {
    faculty = 'Slytherin';
} else if (isKind) {
    faculty = 'Hufflepuff';
} else {
    faculty = 'Gryffindor';
}

alert(faculty);

let finishAge;
finishAge = Number(age) + 6;

alert('You will finish school at ' + finishAge + '!');