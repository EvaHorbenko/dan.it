/* const user = {
  age: 22,
  name: 'Nick',
  weight: 100,
  height: 197,
};

alert(user.name)

const userAttribute = prompt('What else do yoy want to know?');

if (userAttribute in user) {
  alert(user[userAttribute]);
} else {
  const userAttributeValue = prompt('No such field. How to update?')

  user[userAttribute] = userAttributeValue;
}
console.log(user) */


/* const userA = {
  name: 'Nick',
  age: 22,
  height: 197,
  weight: 180,
  biography: {
    title: 'My biography',
    content: 'I was born ...'
  }
}
      // object in object
// console.log(userA.biography.title)


      // copy object, change one part
// const userB = {name: userA.name, age: userA.age, height: userA.height,};
// const userB = Object.assign({}, userA);
const userB = {...userA, biography: {...userA.biography}};
userB.biography.title = 'New title'
userB.name = 'Bob'
// console.log(userA, userB)

for (let key in userA) {
  // console.log(userA[key])
  // console.log(key)
}

const userC = {}
for (let key in userA) {
  userC[key] = userA[key]
}
// console.log(userC) */

/* const user = {
  firstName: 'Leonardo',
  lastName: 'Da Vinci',
  job: 'Full Stack',
  
  sayHi: function () {
    console.log(`Hi, ${this.firstName} ${this.lastName}!`)
  }
}
user.sayHi() */

const students = [];
const graduatedStudents = [];
const nonGraduatedStudents = [];

const addUser = function () {
  const firstName = firstNameInput.value;
  const lastName = lastNameInput.value;
  const grade = gradeInput.value;
  const job = jobInput.value;
  const user = {
    firstName,
    lastName,
    grade,
    job,
  }
  students.push(user)
  // console.log(user)
  console.log(students)
}


const checkWhoGraduated = function () {
  for (let i = 0; i < students.length; i++) {
    if (students[i].grade < 6) {
      nonGraduatedStudents.push(students[i]);
    } else {
      graduatedStudents.push(students[i]);
    }
  }
  /* const firstGraduatedStudent = graduatedStudents[0];
  graduatedStudentsList.innerHTML = firstGraduatedStudent.firstName + ' ' + firstGraduatedStudent.lastName; */
  let studentsName = '';
  for (let student of graduatedStudents) {
    studentsName += student.firstName + ' ' + student.lastName + ', ';
  }
  graduatedStudentsList.innerHTML = studentsName;
}

