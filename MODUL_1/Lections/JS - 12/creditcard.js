const creditCardInput = document.querySelector('.js-creditCardInput');

const ALLOWED_SUMBOLS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']


const handleInpyt = function (event) {
    if (!ALLOWED_SUMBOLS.includes(event.key)) {
        event.preventDefault()
        return
    }
    
    const valueNumbers = creditCardInput.value.split('').filter(sumbol => sumbol !== ' ');

    const isNotEmpty = valueNumbers.length !== 0
    const isDividedByFour = valueNumbers.length % 4 === 0
    const isNotFullInput = valueNumbers.length < 16
    const hasSpace = creditCardInput.value[creditCardInput.value.length - 1] === ' '

    if (isNotEmpty && isDividedByFour && isNotFullInput && !hasSpace) {
        creditCardInput.value += ' ';
    }
    
    if (valueNumbers.length === 16) {
        event.preventDefault()
    }
}

creditCardInput.addEventListener('keypress', handleInpyt)