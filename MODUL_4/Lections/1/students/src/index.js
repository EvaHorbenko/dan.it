import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/classwork_6/App.jsx';
// import TwoForms from './components/TwoForms.jsx';
import User from './components/classwork_3/User.jsx';
import StudentsList from './components/classwork_3_1/StudentsList.jsx';
import ChangingTiles from './components/classwork_4/ChangingTiles.jsx';
import Tiles from './components/classwork_4/Tiles.jsx';
import FormWithValidation from './components/classwork_5/FormWithValidation.jsx';
import FormWithValidationFunction from './components/classwork_5/FormWithValidationFunction.jsx';
import Students from './components/classwork_5/StudentsHook.jsx';
import Fruits from './components/classwork_7/Fruits.jsx';
import Laptops from './components/classwork_7/Laptops.jsx';
import './index.css';

import reportWebVitals from './reportWebVitals';
// import ShoppingList from './components/ShoppingList.jsx';

ReactDOM.render(
  <React.StrictMode>
    {/* <ShoppingList /> CW1 */}
    {/* <TwoForms /> CW2 */}
    {/* <User /> CW3_1 */}
    {/* <User /> CW3_2 */}
    {/* <Tiles /> */}
    {/* <ChangingTiles /> */}
    {/* <FormWithValidationFunction /> */}
    {/* <Students /> CW5 */}
    <App />
    {/* <Fruits /> */}
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
