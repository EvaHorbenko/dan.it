import { calculateStockItems } from './calculate-stock-items';

test('calculates IN correctly', () => {

  expect(
    calculateStockItems([{ name: 'Apples', number: 1, operation: 'IN' }])
  ).toEqual({ Apples: 1 });
});


test('calculates IN and OUT correctly', () => {
  const result = calculateStockItems([
    { name: 'Apples', number: 1, operation: 'IN' },
    { name: 'Apples', number: 1, operation: 'OUT' }
  ]);

  expect(result).toEqual({ Apples: 0 });
});

test('calculates IN and OUT correctly', () => {
  expect(
    () => {
      calculateStockItems([
        { name: 'Apples', number: 1, operation: 'IN' },
        { name: 'Apples', number: 2, operation: 'OUT' }
      ])
    }
  ).toThrow();
});

test('calculates IN and RESERVE correctly', () => {
  expect(
    calculateStockItems([
      { name: 'Apples', number: 1, operation: 'IN' },
      { name: 'Apples', number: 1, operation: 'RESERVE' }
    ])
  ).toEqual({ Apples: 0 });
});

test('calculates IN and RESERVE correctly', () => {
  expect(
    calculateStockItems([
      { name: 'Apples', number: 1, operation: 'IN' },
      { name: 'Apples', number: 1, operation: 'INBOUND' }
    ])
  ).toEqual({ Apples: 2 });
});
