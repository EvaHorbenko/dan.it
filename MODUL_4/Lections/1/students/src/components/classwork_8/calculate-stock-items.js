export const calculateStockItems = (stockItems) => {
  return stockItems.reduce((acc, logItem) => {
    const currentValue = acc[logItem.name] || 0;
  
    let newValue = currentValue;
    if (logItem.operation === 'IN' || logItem.operation === 'INBOUND') {
      newValue = newValue + logItem.number;
    } else {
      newValue = newValue - logItem.number;
      if (newValue < 0) {
        throw new Error(`Sorry, you can't do this`);
      }
    }

    return {
      ...acc,
      [logItem.name]: newValue,
    }
  }, {});
}
