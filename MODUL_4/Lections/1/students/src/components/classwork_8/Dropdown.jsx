import { useState } from "react";

// const Form = () => {
//   const [selectedId, setSelectedId] = useState();

//   const handleChange = (id) => {
//     setSelectedId(id);
//   }

//   const onSubmit = () => {
//     sendRequest(selectedId);
//   }

//   return (
//     <>
//       <Dropdown options={[]} onChange={handleChange} setSelectedId={setSelectedId} />
//       <button onClick={onSubmit}>Submit</button>
//     </>
//   )
// }

const Dropdown = (props) => {
  const { options, onChange, selectedItemId } = props;
  const [isOpen, setIsOpen] = useState(false);

  const handleChange = (e) => {
    const id = e.target.dataset.id;

    onChange(id);
  }

  const handleClick = () => {
    setIsOpen(!isOpen);
  }

  const selectedItem = options.find((item) => item.id === selectedItemId);

  return (
    <div className="container">
      <div onClick={handleClick} className="selectedItem">{selectedItem.label}</div>
      { isOpen && 
        <ul className="list">
          {
            options.map((option) => {
              <li
                onClick={handleChange}
                className="item" 
                key={option.id} 
                data-id={option.id}
              >{option.label}</li>
            })
          }
        </ul>
      }
    </div>
  )
}

export default Dropdown;
