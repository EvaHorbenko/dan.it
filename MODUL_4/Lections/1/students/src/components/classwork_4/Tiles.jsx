import React from 'react';
import "./Tiles.css"

const generateInitialTiles = () => {
  const rowsArr = Array.from(Array(25))
  return rowsArr.map(() => generateRow())
}

const generateRow = () => {
  return Array.from(Array(25))
    .map(() => Math.random() < 0.5 ? 'black' : 'white')
}

class Tiles extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tiles: generateInitialTiles(),
    }
  }

  changeColor = (e) => {
    const updatedTiles = this.state.tiles.map((row, rowNumber) => {
      return row.map((color, columnNumber) => {
        if (+e.target.dataset.row === rowNumber && +e.target.dataset.column === columnNumber) {
          return color === 'white' ? 'black' : 'white'
        } 
        return color
      })
    })
    
    this.setState({
      tiles: updatedTiles,
    })
  }

  render() {
    console.log(this.state);
    return (
      <div className="tiles-wrapper">
        {this.state.tiles.map((row, rowNumber) => {
          const rowElements = row.map((tileColor, columnNumber) => {
            return <div 
              data-row={rowNumber}
              data-column={columnNumber}
              key={`${rowNumber}${columnNumber}`} 
              onClick={this.changeColor} 
              style={{ backgroundColor: tileColor}} 
              className="tile"
            ></div>
          })
          return <React.Fragment key={rowNumber}>{rowElements}<br/></React.Fragment>
        })}
      </div>
    );
  }
}

export default Tiles;