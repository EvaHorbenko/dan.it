
import React from 'react'
import NumberForm from '../NumberForm.jsx'

class TwoForms extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: ''
    }
  }

  onSubmit = () => {
    localStorage.setItem('value', this.state.value)
    this.setState({
      value: ''
    })
  }

  onChange = (event) => {
    if (Number.isInteger(+event.target.value)) {
      this.setState({
        value: event.target.value
      })
    }
  }

  render() {
    return (
      <>
        <NumberForm value={this.state.value} onSubmit={this.onSubmit} onChange={this.onChange} />
        <br /> <br />
        <NumberForm value={this.state.value} onSubmit={this.onSubmit} onChange={this.onChange} />
      </>
    )
  }
}

export default TwoForms;
