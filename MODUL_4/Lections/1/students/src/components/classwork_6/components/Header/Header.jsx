import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../containers/UserContainer/UserContainer";
import ThemesDropdown from "../ThemesDropdown/ThemesDropdown";

const Header = () => {

    const { onLogOut } = useContext(UserContext)

    return (
        <header style={{
            display: 'flex',
            justifyContent: 'space-between',
            backgroundColor: 'blue',
            padding: '10px 20px'
        }}>
            <ThemesDropdown />
            <button onClick={onLogOut}>Log Out</button>
        </header>
    )
}

export default Header