import React, { useContext } from 'react';
import { ThemeContext } from '../ThemeContext/ThemeContext';


const ThemesDropdown = () => {
  const {themeName, onChange} = useContext(ThemeContext)
  
  return (
    <select value={themeName} onChange={onChange}>
      <option value="LIGHT">Light</option>
      <option value="BLUE">Blue</option>
    </select>
  )
  
}

export default ThemesDropdown;