import React, { useContext, useEffect, useRef, useState } from 'react'
import { Link, useParams } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import { API_SERVER } from '../../config';
import { StudentsContext } from '../StudentsContext/StudentsContext';
import { ThemeContext } from '../ThemeContext/ThemeContext';

const useFetchedStudent = (id) => {
  const [student, setStudent] = useState(null);
  const { students } = useContext(StudentsContext);

  useEffect(() => {
    const student = students?.find(student => student.id === Number(id))

    setStudent(student);
  }, []);

  useEffect(() => {
    if (student === undefined) {
      fetch(`${API_SERVER}/student/${id}`)
        .then(response => response.json())
        .then(response => setStudent(response))
    }
  }, [student, id]);

  return student;
}

const EditStudent = () => {
  const {theme} = useContext(ThemeContext);
  const { onEdit } = useContext(StudentsContext);
  const params = useParams();
  const [studentToEdit, setStudentToEdit] = useState(null);
  const navigate = useNavigate();
  const student = useFetchedStudent(params.id);

  useEffect(() => {
    console.log(studentToEdit);
    if (!studentToEdit) {
      setStudentToEdit(student);
    }
    
  }, [student]);

  const onChangeFirstName = (e) => {
    setStudentToEdit({
      ...studentToEdit,
      firstName: e.target.value,
    });
  }

  const onChangeLastName = (e) => {
    setStudentToEdit({
      ...studentToEdit,
      lastName: e.target.value,
    });
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    fetch(`${API_SERVER}/student/${studentToEdit.id}`, {
      method: 'PATCH',
      headers: {'Content-type' : 'application/json'},
      body: JSON.stringify({
        firstName: studentToEdit.firstName,
        lastName: studentToEdit.lastName,
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        onEdit(response);
        navigate("/");
      })
  }

  return (
    <div>
      <h1>Edit student</h1>
      <form>
        <label>
          First Name
          <input value={studentToEdit?.firstName} onChange={onChangeFirstName} />
        </label>
        <label>
          Last Name
          <input value={studentToEdit?.lastName} onChange={onChangeLastName} />
        </label>
        <button style={theme} onClick={handleSubmit}>Submit</button>
      </form>
      
    </div>

  );
}

export default EditStudent