import React, { useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { API_SERVER } from '../../config'
import StudentFullName from '../StudentFullName/StudentFullName';
import Header from '../Header/Header';
import { StudentsContext } from '../StudentsContext/StudentsContext';


const useFetchedStudent = (id) => {
  const [student, setStudent] = useState(null);
  const { students } = useContext(StudentsContext);

  useEffect(() => {
    const student = students?.find(student => student.id === Number(id))

    setStudent(student);
  }, []);

  useEffect(() => {
    if (student === undefined) {
      fetch(`${API_SERVER}/student/${id}`)
        .then(response => response.json())
        .then(response => setStudent(response))
    }
  }, [student, id]);

  return student;
}


const StudentDetails = () => {
  const params = useParams();

  const student = useFetchedStudent(params.id);

  return(
    <>
      <Header />
      <h1>StudentDetails</h1>
      {student && <StudentFullName student={student} />}
    </>
  )
}

export default StudentDetails