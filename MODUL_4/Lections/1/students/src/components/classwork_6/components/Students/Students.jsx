import React, { useContext, useEffect } from 'react';
import { Link } from "react-router-dom";
import { API_SERVER } from '../../config';
import { StudentsContext } from '../StudentsContext/StudentsContext';
import { ThemeContext } from '../ThemeContext/ThemeContext';
import Header from '../Header/Header';


const Students = () => {
  const { 
    onDelete, 
    onCollectionFirstRender, 
    students 
  } = useContext(StudentsContext);
  const {theme} = useContext(ThemeContext);

  useEffect(onCollectionFirstRender, []);

  const deleteUser = (e) => {
    fetch(`${API_SERVER}/student/${e.target.dataset.id}`, {
      method: 'DELETE'
    })
     .then(() => {
        onDelete(+e.target.dataset.id);
     })
  }
console.log(theme);
  return (
    <div>
      <Header />
      <h1>Students list</h1>
      {students && students.map(student => {
        return <div key={student.id}> 
          {student.firstName} {student.lastName} 
          <button style={theme} data-id={student.id} onClick={deleteUser} >X</button>
          <Link to={`/student-details/${student.id}`}>
            <button style={theme}> Student details </button>
          </Link>
          <Link to={`/edit-student/${student.id}`}>
            <button style={theme}> Edit student</button>
          </Link>
        </div>
      })}
      <Link to="/create-student">
        <button style={theme}>Create student</button>
      </Link>
    </div> 
  );
}

export default Students;
