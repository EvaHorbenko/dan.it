import React, { useContext, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../containers/UserContainer/UserContainer";

const Login = () => {

    const emailRef = useRef()
    const passwordRef = useRef()
    const navigate = useNavigate();
    const {onLogIn} = useContext(UserContext)

    const handleSubmit = (event) => {
        event.preventDefault()
        onLogIn(
            emailRef.current.value, 
            passwordRef.current.value, 
            () => {
                navigate("/")
            }
        )
    }

    return (
        <>
            <h1>Please Log In</h1>
            <form onSubmit={handleSubmit}>
                <label> E-mail
                    <input ref={emailRef} type="email" />
                </label>
                <label> Password
                    <input ref={passwordRef} type="password" />
                </label>
                <button type="submit">Submit</button>
            </form>
        </>
    )
}

export default Login