import React from 'react'

const StudentFullName = (props) => {
  const { student } = props

  return (<>
    <div>First name: {student.firstName}</div>
    <div>Last name: {student.lastName}</div>
  </>)
}

export default StudentFullName 