import React, { useContext, useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import CreateStudent from './components/CreateStudent/CreateStudent';
import EditStudent from './components/EditStudent/EditStudent';
import StudentDetails from './components/StudentDetails/StudentDetails';
import Students from './components/Students/Students';
import { StudentsContext } from './components/StudentsContext/StudentsContext';
import { ThemeContext } from './components/ThemeContext/ThemeContext';
import ThemesDropdown from './components/ThemesDropdown/ThemesDropdown';
import { API_SERVER } from './config';
import StudentsContainer from './containers/StudentsContainer/StudentsContainer';
import ThemesContainer from './containers/ThemesContainer/ThemesContainer';
import Login from './components/Login/Login'
import UserContainer from './containers/UserContainer/UserContainer';



const App = () => {

  return (
    <BrowserRouter>
      <UserContainer>
        <ThemesContainer>
          <StudentsContainer>

            <Routes>
              <Route path="login" element={
                <Login />
              } />
              <Route path="/" element={
                <Students />
              } />
              <Route path="create-student" element={
                <CreateStudent />
              } />
              <Route path="edit-student/:id" element={
                <EditStudent />
              } />
              <Route path="student-details/:id" element={
                <StudentDetails />
              } />
            </Routes>
          </StudentsContainer>
        </ThemesContainer>
      </UserContainer>
    </BrowserRouter>
  );
}

export default App;
