import React, { useContext, useEffect, useState } from 'react';
import { StudentsContext } from '../../components/StudentsContext/StudentsContext';
import { API_SERVER } from '../../config';
import { UserContext } from '../UserContainer/UserContainer';

const StudentsContainer = (props) => {
  const { isLogedIn } = useContext(UserContext)
  const [students, setStudents] = useState(null);

  useEffect(() => {
    if (!isLogedIn) {
      setStudents(null)
    }
  }, [isLogedIn])

  const handleCollectionFirstRender = () => {
    if (students) {
      return;
    }
    if (!isLogedIn) {
      return;
    }
    
    fetch(`${API_SERVER}/student`)
      .then(res => res.json())
      .then(res => {
        setStudents(res);
      })
  }

  const handleDelete = (id) => {
    const newStudents = students.filter((student) => {
      if (student.id === id) {
        return false
      } else {
        return true
      }
    })
    setStudents(newStudents)
  }

  const handleCreate = (newStudent) => {
    setStudents([...students, newStudent]);
  }

  const handleEdit = (updatedStudent) => {
    const newStudents = students.map((student) => {
      if (student.id === updatedStudent.id) {
        return updatedStudent;
      }
      return student;
    });

    setStudents(newStudents);
  }

  const { children } = props;

  return (
    <StudentsContext.Provider value={{
      students: students,
      onCreate: handleCreate,
      onDelete: handleDelete,
      onEdit: handleEdit,
      onCollectionFirstRender: handleCollectionFirstRender,
    }}>
      {children}
    </StudentsContext.Provider>
  );
}

export default StudentsContainer;