import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

export const UserContext = React.createContext(null)

const UserContainer = (props) => {
    const [isLogedIn, setIsLogedIn] = useState(false)
    const navigate = useNavigate()

    useEffect(() => {
        const isLogedInLS = Boolean(localStorage.getItem('isLogedIn'))
        setIsLogedIn(isLogedInLS)
    }, [])

    const handleLogIn = (email, password, onSuccess, onError) => {
        setTimeout(() => {
            localStorage.setItem('isLogedIn', 'true')
            setIsLogedIn(true)
            onSuccess()
        }, 2000)
    }

    const handleLogOut = () => {
        localStorage.removeItem('isLogedIn')
        setIsLogedIn(false)
        navigate("/login")
    }

    const { children } = props;

    return (
        <UserContext.Provider value={{
            isLogedIn: isLogedIn,
            onLogIn: handleLogIn,
            onLogOut: handleLogOut,

        }}>
            {children}
        </UserContext.Provider>
    );
}

export default UserContainer;