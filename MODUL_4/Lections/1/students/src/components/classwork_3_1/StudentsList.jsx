import React from 'react'

class StudentsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      requestState: 'idle',
      studentsList: []
    }
  }

  componentDidMount() {
    this.setState({
      requestState: 'loading'
    })
    fetch('https://5948-217-25-198-197.ngrok.io/student')
      .then(response => response.json())
      .then(response => {
        setTimeout( () => {
          this.setState({
            requestState: 'success',
            studentsList: response
          })
        }, 2000)
      })
      .catch( () => this.setState({
        requestState: 'error'
      }))
  }

  deleteStudent = (id) => {
    fetch(`https://5948-217-25-198-197.ngrok.io/student/${id}`, {
      method: 'DELETE'
    })
  } 

  render() {
    if(this.state.requestState === 'loading') {
      return (
        <span>Loading</span>
      )
    }
    if(this.state.requestState === 'error') {
      return (
        <span>Sorry, somothing went wrong</span>
      )
    }
    if(this.state.requestState === 'idle') {
      return null
    }

    return (
      <ul>
        {this.state.studentsList.map(student => {
          return <li key={student.id}>
            {student.firstName} {student.lastName} 
            <button onClick={() => {this.deleteStudent(student.id)}}>Delete</button>
          </li>
        })}
      </ul>
    )
  }
}

export default StudentsList