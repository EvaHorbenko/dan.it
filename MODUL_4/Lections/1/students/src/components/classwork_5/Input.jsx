import React from 'react';
import './Input.css';

function Input(props) {
  const { value, onChange, error, label } = props;

  return (
    <label className="label">
      {label}
      <input
        className="input" 
        value={value} 
        onChange={onChange}
      />
      { error && 
        <span className="error">
          {error}
        </span>
      } 
    </label>
  );
}

export default Input;
