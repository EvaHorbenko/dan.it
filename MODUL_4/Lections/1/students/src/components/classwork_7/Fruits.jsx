
const FRUITS_LOG = [
  {
    name: 'Bananas',
    operation: 'IN',
    number: 10,
  },
  {
    name: 'Apples',
    operation: 'IN',
    number: 11,
  },
  {
    name: 'Oranges',
    operation: 'IN',
    number: 30,
  },
  {
    name: 'Bananas',
    operation: 'OUT',
    number: 5,
  },
  {
    name: 'Apples',
    operation: 'OUT',
    number: 2,
  },
  {
    name: 'Bananas',
    operation: 'IN',
    number: 3,
  },
  {
    name: 'Apples',
    operation: 'OUT',
    number: 4,
  },
  {
    name: 'Grape',
    operation: 'IN',
    number: 17,
  },
]

const Fruits = () => {
  const fruitsData = FRUITS_LOG.reduce((acc, logItem) => {
    const currentValue = acc[logItem.name] || 0;

    let newValue = currentValue;
    if (logItem.operation === 'IN') {
      newValue = newValue + logItem.number;
    } else {
      newValue = newValue - logItem.number;
    }

    return {
      ...acc,
      [logItem.name]: newValue,
    }
  }, {});

  return (
    <ul>
      {
        Object.entries(fruitsData).map(([key, value]) => {
          return (<li>{key}: {value}</li>);
        })
      }
      {/* <li>Bananas: {fruitsData.Bananas}</li>
      <li>Oranges: {fruitsData.Oranges}</li>
      <li>Apples: {fruitsData.Apples}</li> */}
    </ul>
  )
}

export default Fruits;