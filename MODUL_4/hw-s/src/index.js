import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import reportWebVitals from './reportWebVitals';

// import ModalWindows from './components/HW1_modal-windows/ModalWindow';
// import App from './components/HW2_online-shop/App';
import App from './components/HW3_online-shop__with-Cart-and-WishList/App';

ReactDOM.render(
  <React.StrictMode>
    {/* <ModalWindows />   HW-1 */}  
    {/* <App />      HW-2 */}
    <App />

  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
