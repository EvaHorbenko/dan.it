import React from 'react';
import PropTypes from 'prop-types';
import '../Button/Button.scss'

const Button = (props) => {

    const { elementClass, action, onClick, content, afterContent } = props

    return (
        <button 
            className={`button ${elementClass}`} 
            data-action={action}
            onClick={onClick}
            >
            {content}{afterContent}
        </button>
    )
}

Button.propTypes = {
    elementClass: PropTypes.string,
    action: PropTypes.bool,
    onClick: PropTypes.func,
    content: PropTypes.node,
    afterContent: PropTypes.node,
}

Button.defaultProps = {
    action: false,
    content: '',
    afterContent: '',
}

export default Button