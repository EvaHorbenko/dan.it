import React from "react";

import ProductCard from "../ProductCard/ProductCard";


const WishList = (props) => {

    const { productsListToRender, handleShowModal, onChangeSelectStatus } = props
    
    return (
        <>
            <h1 className="main__title">избранное</h1>

            <ul className="main__products-wrapper">
                {productsListToRender?.map(product => {
                    return <ProductCard
                        key={product.id}
                        id={product.id}
                        title={product.title}
                        isInWishList={product.isInWishList}
                        isInCart={product.isInCart}
                        price={product.price}
                        color={product.color}
                        imgName={product.imgName}
                        onChangeSelectStatus={onChangeSelectStatus}
                        handleShowModal={() => {handleShowModal(product.id)}}
                    />
                })}
            </ul>
        </>
    )
}

export default WishList