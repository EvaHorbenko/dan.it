import React from "react";
import { BrowserRouter } from "react-router-dom"

import Header from './Header/Header'
import Footer from './Footer/Footer'
import ProductsManager from "./ProductsManager/ProductsManager";

import './shortReset.scss'
import './variables_&_mixins.scss'
import './ProductsManager/ProductsManager.scss'


const App = () => {

    return (
            <BrowserRouter>
                <Header />
                <ProductsManager />
                <Footer />
            </BrowserRouter>
    )
}

export default App