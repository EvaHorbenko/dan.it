import React from "react";
import PropTypes from 'prop-types';

import Button from "../Button/Button";
import '../Modal/Modal.scss';
import './ProductCard.scss'

const ProductCard = (props) => {

    const { id, title, isInWishList, onChangeSelectStatus, imgName, color, isInCart, handleShowModal, price } = props

    return (
        <li className="product-card" data-id={id}>
            <h2 className="product-card__title">{title}</h2>
            <Button
                content={isInWishList ? <i className="button__icon fas fa-star"></i> : <i className="button__icon far fa-star"></i>}
                elementClass="button__selected"
                onClick={onChangeSelectStatus}
            />
            <img className="product-card__img" src={`./HW-2/imgs/${imgName}`} alt="" />
            <div className="product-card__color">Цвет: {color}</div>
            <Button 
                content={isInCart ? "Удалить из кoрзины" : "Добавить в корзину"}
                afterContent={isInCart ? <i className="button__icon button__cart--icon far fa-trash-alt"></i> : <i className="button__icon button__cart--icon fas fa-cart-arrow-down"></i>}
                elementClass="button__cart"
                onClick={handleShowModal}
            />
            <div className="product-card__price">{price} грн</div>
        </li>
    )
}

ProductCard.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    isInWishList: PropTypes.bool,
    isInCart: PropTypes.bool,
    price: PropTypes.number,
    color: PropTypes.string,
    imgName: PropTypes.string,
    onChangeSelectStatus: PropTypes.func,
    onChangeCartStatus: PropTypes.func,
}

export default ProductCard