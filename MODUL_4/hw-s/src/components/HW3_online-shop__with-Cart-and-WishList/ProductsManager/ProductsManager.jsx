import React, { useState, useEffect } from "react";
import RoutesContainer from '../RoutesContainer/RoutesContainer'

import ModalCollection from '../Modal/ModalCollection';
import Modal from "../Modal/Modal";

import './ProductsManager.scss'

const ProductsManager = () => {

    const [allProductsList, setProductsList] = useState(null)

    const [productWishListId, setProductWishListId] = useState(null)
    const [productInCartId, setProductInCartId] = useState(null)

    const [isModalOpen, setIsModalOpen] = useState(false)
    const [modalToRender, setModalToRender] = useState({})
    const [currentProductId, setCurrentProductId] = useState(null)


    useEffect(() => {
        if (!allProductsList) {
            fetch('./HW-2/ItemsCollection.json')
            .then(response => response.json())
            .then(response => setProductsList(response))
        }
        if (!productWishListId || !productInCartId) {
            checkLocalStorage()
        }
    }, [allProductsList])


    const checkLocalStorage = () => {
        
        let productWishListIdFromLS = JSON.parse(localStorage.getItem('product-in-wishlist-id'))
        if (productWishListIdFromLS && allProductsList) {
            productWishListIdFromLS.forEach(productId => {
                updateCurrentListState(productId, 'isInWishList', productWishListIdFromLS, setProductWishListId, 'product-in-wishlist-id')
            })
        }

        let productInCartId = JSON.parse(localStorage.getItem('product-in-cart-id'))
        if (productInCartId && allProductsList) {
            productInCartId.forEach(productId => {
                updateCurrentListState(productId, 'isInCart', productInCartId, setProductInCartId, 'product-in-cart-id')
            })
        }
    }

    const getIsInWishListStatus = (event) => {
        let targetId = event.target.closest('.product-card').dataset.id
        updateCurrentListState(targetId, 'isInWishList', productWishListId, setProductWishListId, 'product-in-wishlist-id')
    }

    const getIsInCartStatus = (event) => {
        const targetAction = event.target.dataset.action
        if (targetAction === true.toString()) {
            const targetId = currentProductId
            updateCurrentListState(targetId, 'isInCart', productInCartId, setProductInCartId, 'product-in-cart-id')
        }
    }

    const updateCurrentListState = (currentId, keyToUpdate, productCurrentListId, setProductCurrentListId, LSKey) => {
        const productToUpdate = allProductsList.find(product => Number(product.id) === Number(currentId))
        productToUpdate[keyToUpdate] = productToUpdate[keyToUpdate] ? false : true

        const productId = productCurrentListId?.find(id => Number(productToUpdate.id) === Number(id))
        
        let updatedCurrentListState = []
        if (productToUpdate[keyToUpdate]) {
            if (!productId) {
                updatedCurrentListState = [ ...productCurrentListId || updatedCurrentListState || [], productToUpdate.id]
            } else {
                updatedCurrentListState = productCurrentListId
            }
        } else {
            updatedCurrentListState = productCurrentListId.filter(productId => Number(productId) !== Number(productToUpdate.id))
        }

        setProductCurrentListId(updatedCurrentListState)
        localStorage.setItem(LSKey, JSON.stringify(updatedCurrentListState))
    }

    const showModal = (id) => {
        const currentProduct = allProductsList.find(product => product.id === id)
        const modalToRender = ModalCollection.filter(modal => modal.toAddInCart !== currentProduct.isInCart)[0]
        
        setIsModalOpen(true)
        setModalToRender(modalToRender)
        setCurrentProductId(currentProduct.id)
    }

    const deleteModal = (event) => {
        let target = event.target
        if(target.classList.contains('js-close-btn') || target.classList.contains('modal__wrapper')) {
            setIsModalOpen(false)
            setModalToRender({})
            setCurrentProductId(null)
        }
    }


    return (
        <main className="main">
            <RoutesContainer 
                allProductsList={allProductsList}
                productWishListId={productWishListId}
                productInCartId={productInCartId}
                getIsInWishListStatus={getIsInWishListStatus}
                handleShowModal={(productId) => {showModal(productId)}}
            />

            {isModalOpen && <Modal 
                modalClass={modalToRender.modalClass}
                header={modalToRender.header} 
                closeBtn={modalToRender.closeBtn}
                text={modalToRender.text} 
                buttonsAction={modalToRender.buttons}
                onChangeCartStatus={getIsInCartStatus}
                onClick={deleteModal}
            />}
        </main>
    )
}

export default ProductsManager