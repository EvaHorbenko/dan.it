import React from 'react';
import PropTypes from 'prop-types';

import Button from '../Button/Button';

const Modal = (props) => {

    const { modalClass, header, closeBtn, onClick, text, buttonsAction, onChangeCartStatus } = props

    return (
        <div className="modal__wrapper" onClick={onClick}>
            <div className={`modal__inner ${modalClass}`}>
                <div className="modal__header">
                    <h1>{header}</h1>
                    {closeBtn && <i className="far fa-window-close close-icon js-close-btn" onClick={onClick}></i>}
                </div>
                <p className="modal__text">{text}</p>
                <div className="btns-collection">

                    {buttonsAction.map(button => {
                        return <Button key={button.id}
                            elementClass={`modal__button js-close-btn ${button.elementClass}`}
                            content={button.text} 
                            action={button.hasCartAction}
                            onClick={onChangeCartStatus}
                        />
                    })}
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    modalClass: PropTypes.string,
    header: PropTypes.string,
    closeBtn: PropTypes.bool,
    text: PropTypes.string,
    buttonsAction: PropTypes.array,
    onChangeCartStatus: PropTypes.func,
    onClick: PropTypes.func,
}

export default Modal