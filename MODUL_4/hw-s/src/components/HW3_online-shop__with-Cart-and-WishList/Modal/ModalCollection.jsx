const ModalCollection = [
    {   id: 1,
        toAddInCart: true,
        modalClass: 'modal__add',
        header: 'Добавить товар в корзину',
        closeBtn: true,
        text: 'Вы уверены, что хотитие добавить товар в корзину?',
        buttons: [
            {
                id: 1,
                text: 'Да, добавить',
                hasCartAction: true,
                elementClass: "modal__button--add"
            }, 
            {
                id: 2,
                text: 'Нет, не добавлять',
                hasCartAction: false,
                elementClass: "modal__button--add"
            }, 
        ],
    }, 
    
    {   id: 2,
        toAddInCart: false,
        modalClass: 'modal__remove',
        header: 'Удалить товар из корзины',
        closeBtn: false,
        text: 'Вы уверены, что хотитие удалить товар из корзины?',
        buttons: [
            {
                id: 1,
                text: 'Да, удалить',
                hasCartAction: true,
                elementClass: "modal__button--remove"
            }, 
            {
                id: 2,
                text: 'Нет, оставить',
                hasCartAction: false,
                elementClass: "modal__button--remove"
            }, 
        ],
    }, 

]

export default ModalCollection