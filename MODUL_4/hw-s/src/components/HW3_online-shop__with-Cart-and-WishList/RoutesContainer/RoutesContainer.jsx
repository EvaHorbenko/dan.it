import React from "react";
import { Route, Routes } from "react-router-dom"

import ProductsList from '../ProductsList/ProductsList'
import Cart from '../Cart/Cart'
import WishList from '../WishList/WishList'

const RoutesContainer = (props) => {

    const {allProductsList, productWishListId, productInCartId, getIsInWishListStatus, handleShowModal} = props

    const getProductsById = (idList) => {
        let currentListProducts = []
        allProductsList?.forEach(product => {
            idList?.forEach(currentId => {
                if (product.id === currentId) {
                    currentListProducts.push(product)
                }
            })
        })
        return currentListProducts
    }

    return (
        <Routes>
            <Route path="/" element={
                <ProductsList 
                    productsListToRender={allProductsList} 
                    onChangeSelectStatus={getIsInWishListStatus}
                    handleShowModal={(productId) => {handleShowModal(productId)}}
                />
            } />
            <Route path="wish-list" element={
                <WishList 
                    productsListToRender={getProductsById(productWishListId)} 
                    onChangeSelectStatus={getIsInWishListStatus}
                    handleShowModal={(productId) => {handleShowModal(productId)}}
                />
            } />
            <Route path="cart" element={
                <Cart 
                    productsListToRender={getProductsById(productInCartId)} 
                    onChangeSelectStatus={getIsInWishListStatus}
                    handleShowModal={(productId) => {handleShowModal(productId)}}
                />
            } />

        </Routes>
    )
}

export default RoutesContainer