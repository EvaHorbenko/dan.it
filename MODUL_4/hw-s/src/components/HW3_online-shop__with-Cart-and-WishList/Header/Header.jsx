import React from 'react'
import '../Header/Header.scss'

import { Link } from 'react-router-dom'

const Header = () => {
    return (
        <header className="header">
            <Link to="/">
                <h1 className="header__title">LoftStore</h1>
            </Link>

            <ul className='header__link-list'>
                <Link to="/">
                    <li className='header__link'>домой</li>
                </Link>
                <Link to="wish-list">
                    <li className='header__link'>избранное</li>
                </Link>
                <Link to="cart">
                    <li className='header__link'>корзина</li>
                </Link>

            </ul>
        </header>
    )
}

export default Header