import React from 'react'
import '../Header/Header.scss'

class Header extends React.Component {
    render() {
        return (
            <header className="header">
                <h1 className="header__title">LoftStore</h1>
            </header>
        )
    }
}

export default Header