import React from 'react'
import Header from './Header/Header'
import ProductsList from './ProductsList/ProductsList'
import Footer from './Footer/Footer'

class App extends React.Component {
    render() {
        return (
            <>
                <Header />
                <ProductsList />
                <Footer />
            </>
        )
    }
}

export default App