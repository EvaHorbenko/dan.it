import React from 'react';
import PropTypes from 'prop-types';
import '../Button/Button.scss'

class Button extends React.Component {
    render() {
        return (
            <button 
                className={`button ${this.props.elementClass}`} 
                data-action={this.props.action}
                onClick={this.props.onClick}
                >
                {this.props.content}{this.props.afterContent}
            </button>
        )
    }
}

Button.propTypes = {
    elementClass: PropTypes.string,
    action: PropTypes.bool,
    onClick: PropTypes.func,
    content: PropTypes.node,
    afterContent: PropTypes.node,
}

Button.defaultProps = {
    action: false,
    content: '',
    afterContent: '',
}

export default Button