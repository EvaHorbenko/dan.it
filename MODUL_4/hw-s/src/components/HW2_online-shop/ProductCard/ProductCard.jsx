import React from "react";
import PropTypes from 'prop-types';

import Button from "../Button/Button";
import '../Modal/Modal.scss';
import './ProductCard.scss'

class ProductCard extends React.Component {
    render() {
        return (
            <li className="product-card" data-id={this.props.id}>
                <h2 className="product-card__title">{this.props.title}</h2>
                <Button
                    content={this.props.isInWishList ? <i className="button__icon fas fa-star"></i> : <i className="button__icon far fa-star"></i>}
                    elementClass="button__selected"
                    onClick={this.props.onChangeSelectStatus}
                />
                <img className="product-card__img" src={`./HW-2/imgs/${this.props.imgName}`} alt="" />
                <div className="product-card__color">Цвет: {this.props.color}</div>
                <Button 
                    content={this.props.isInCart ? "Удалить из кoрзины" : "Добавить в корзину"}
                    afterContent={this.props.isInCart ? <i className="button__icon button__cart--icon far fa-trash-alt"></i> : <i className="button__icon button__cart--icon fas fa-cart-arrow-down"></i>}
                    elementClass="button__cart"
                    onClick={this.props.handleShowModal}
                />
                <div className="product-card__price">{this.props.price} грн</div>
            </li>
        )
    }
}

ProductCard.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    isInWishList: PropTypes.bool,
    isInCart: PropTypes.bool,
    price: PropTypes.number,
    color: PropTypes.string,
    imgName: PropTypes.string,
    onChangeSelectStatus: PropTypes.func,
    onChangeCartStatus: PropTypes.func,
}

export default ProductCard