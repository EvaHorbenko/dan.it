import React from "react";
import './ProductsList.scss'
import ProductCard from "../ProductCard/ProductCard";
import ModalCollection from '../Modal/ModalCollection';
import Modal from "../Modal/Modal";

class ProductsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            productsList: [],
            productWishListId: [],
            productInCartId: [],
            isModalOpen: false,
            modalToRender: {},
            currentProductId: null,
        }
    }

    componentDidMount = () => {
        fetch('./HW-2/ItemsCollection.json')
            .then(response => response.json())
            .then(response => this.setState({
                productsList: response
            }))
            .then(() => this.checkLocalStorage())
    }

    checkLocalStorage = () => {
        let productWishListId = JSON.parse(localStorage.getItem('product-in-wishlist-id'))
        if (productWishListId) {
            productWishListId.forEach(productId => {
                this.updateProductState(productId, 'isInWishList', 'product-in-wishlist-id', 'productWishListId')
            })
        }
        let productInCartId = JSON.parse(localStorage.getItem('product-in-cart-id'))
        if (productInCartId) {
            productInCartId.forEach(productId => {
                this.updateProductState(productId, 'isInCart', 'product-in-cart-id', 'productInCartId')
            })
        }
    }

    getIsInWishListStatus = (event) => {
        let targetId = event.target.closest('.product-card').dataset.id
        this.updateProductState(targetId, 'isInWishList', 'product-in-wishlist-id', 'productWishListId')
    }

    getIsInCartStatus = (event) => {
        const targetAction = event.target.dataset.action
        if (targetAction === true.toString()) {
            const targetId = this.state.currentProductId
            this.updateProductState(targetId, 'isInCart', 'product-in-cart-id', 'productInCartId')
        }
    }

    updateProductState = (currentId, keyToCheck, keyLS, stateToSet) => {
        const productToChange = this.state.productsList.find(product => Number(product.id) === Number(currentId))
        productToChange[keyToCheck] = productToChange[keyToCheck] ? false : true

        let updatedProductState

        if (productToChange[keyToCheck]) {
            updatedProductState = [ ...this.state[stateToSet], productToChange.id]
        } else {
            updatedProductState = this.state[stateToSet].filter(productId => Number(productId) !== Number(productToChange.id))
        }

        this.setUpdatedProductState(keyLS, stateToSet, updatedProductState)        
    }

    setUpdatedProductState = (keyLS, stateToSet, updatedProductState) => {
        this.setState(() => {
            return {[stateToSet]: updatedProductState}
        }, () => {
            localStorage.setItem(keyLS, JSON.stringify(this.state[stateToSet]))
        })
    }

    showModal = (id) => {
        const currentProduct = this.state.productsList.find(product => product.id === id)
        const modalToRender = ModalCollection.filter(modal => modal.toAddInCart !== currentProduct.isInCart)[0]
        this.setState({
            isModalOpen: true,
            modalToRender: modalToRender,
            currentProductId: currentProduct.id,
        })
    }

    deleteModal = (event) => {
        console.log('deleteModal');
        let target = event.target
        if(target.classList.contains('js-close-btn') || target.classList.contains('modal__wrapper')) {
            this.setState({
                isModalOpen: false,
                modalToRender: {},
                currentProductId: null,
            })
        }
    }

    render() {
        return (
            <main className="main">
                <ul className="main__products-wrapper">
                    {this.state.productsList.map(product => {
                        return <ProductCard
                            key={product.id}
                            id={product.id}
                            title={product.title}
                            isInWishList={product.isInWishList}
                            isInCart={product.isInCart}
                            price={product.price}
                            color={product.color}
                            imgName={product.imgName}
                            onChangeSelectStatus={this.getIsInWishListStatus}
                            handleShowModal={() => {this.showModal(product.id)}}
                        />
                    })}
                    
                    {this.state.isModalOpen && <Modal 
                        modalClass={this.state.modalToRender.modalClass}
                        header={this.state.modalToRender.header} 
                        closeBtn={this.state.modalToRender.closeBtn}
                        text={this.state.modalToRender.text} 
                        buttonsAction={this.state.modalToRender.buttons}
                        onChangeCartStatus={this.getIsInCartStatus}
                        onClick={this.deleteModal}
                    />}
                </ul>
            </main>
        )
    }
}

export default ProductsList