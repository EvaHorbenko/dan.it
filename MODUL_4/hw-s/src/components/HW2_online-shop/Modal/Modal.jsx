import React from 'react';
import PropTypes from 'prop-types';

import Button from '../Button/Button';

class Modal extends React.Component {
    render() {
        return (
            <div className="modal__wrapper" onClick={this.props.onClick}>
                <div className={`modal__inner ${this.props.modalClass}`}>
                    <div className="modal__header">
                        <h1>{this.props.header}</h1>
                        {this.props.closeBtn && <i className="far fa-window-close close-icon js-close-btn button" onClick={this.props.onClick}></i>}
                    </div>
                    <p className="modal__text">{this.props.text}</p>
                    <div className="btns-collection">

                        {this.props.buttonsAction.map(button => {
                            return <Button key={button.id}
                                elementClass={`modal__button js-close-btn ${button.elementClass}`}
                                content={button.text} 
                                action={button.hasCartAction}
                                onClick={this.props.onChangeCartStatus}
                            />
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    modalClass: PropTypes.string,
    header: PropTypes.string,
    closeBtn: PropTypes.bool,
    text: PropTypes.string,
    buttonsAction: PropTypes.array,
    onChangeCartStatus: PropTypes.func,
    onClick: PropTypes.func,
}

export default Modal