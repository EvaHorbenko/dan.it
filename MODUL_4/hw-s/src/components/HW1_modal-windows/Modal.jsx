import React from 'react';
import Button from './Button';

class Modal extends React.Component {
    render() {
        return (
            <div className="modal-wrapper" onClick={this.props.onClick}>
                <div className={`modal-inner ${this.props.modalClass}`}>
                    <div className="header">
                        <h1>{this.props.header}</h1>
                        {this.props.closeBtn && <i class="far fa-window-close close-icon js-close-btn" onClick={this.props.onClick}></i>}
                    </div>
                    <p className="text">{this.props.text}</p>
                    <div className="btns-collection">

                        {this.props.buttonsAction.map(button => {
                            return <Button text={button.text} backgroundColor={button.backgroundColor} onClick={this.props.onClick} />
                        })}

                    </div>
                </div>
            </div>
        )
    }
}

export default Modal