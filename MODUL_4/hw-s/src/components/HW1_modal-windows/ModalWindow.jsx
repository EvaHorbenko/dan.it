import React from 'react';
import Modal from './Modal';
import './ModalWindow.scss';
import ModalCollection from './ModalCollection';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false, 
            modalToRender: {}
        }
    }

    openModal = (event) => {
        let target = event.target
        const modalToRender = ModalCollection.filter(modal => Number(modal.id) === Number(target.dataset.modalId))[0]
        this.setState({
            isModalOpen: true,
            modalToRender: modalToRender
        })
    }
    
    deleteModal = (event) => {
        let target = event.target
        if(target.classList.contains('js-close-btn') || target.classList.contains('modal-wrapper')) {
            this.setState({
                isModalOpen: false,
                modalToRender: {}
            })
        }
    }

    render() {
        return (
            <main className="main">
                <button data-modal-id="1" className="openModalBtn button" onClick={this.openModal}>Open first modal</button>
                <button data-modal-id="2" className="openModalBtn button" onClick={this.openModal}>Open second modal</button>

                {this.state.isModalOpen && <Modal 
                    modalClass={this.state.modalToRender.modalClass}
                    header={this.state.modalToRender.header} 
                    closeBtn={this.state.modalToRender.closeBtn}
                    text={this.state.modalToRender.text} 
                    buttonsAction={this.state.modalToRender.buttons}
                    onClick={this.deleteModal}
                />}

            </main>
        );
    }
}

export default Main;
