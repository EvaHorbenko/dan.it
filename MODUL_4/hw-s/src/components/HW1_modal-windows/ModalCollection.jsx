const ModalCollection = [
    {   id: 1,
        modalClass: 'redModal',
        header: 'Do you want to delete this file?',
        closeBtn: true,
        text: 'Once you delete this file, it won`t be possible to to undo this action. Are you sure you want to delete it?',
        buttons: [
            {   backgroundColor: '#b52e2e',
                text: 'Ok',
            }, 
            {   backgroundColor: '#ff6c6c',
                text: 'Cancel',
            }, 
        ],
    }, 
    
    {   id: 2,
        modalClass: 'greenModal',
        header: 'Do you want to do something else with this file?',
        closeBtn: false,
        text: 'Are you sure you want do something else?',
        buttons: [
            {   backgroundColor: '#0e6f66',
                text: 'Yes',
            }, 
            {   backgroundColor: '#4fdbce',
                text: 'No',
            }, 
        ],
    }, 

]

export default ModalCollection