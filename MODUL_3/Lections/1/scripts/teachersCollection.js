const teacherList = [];

function TeachersCollection () {
  this.list = teacherList;
  this.firstNameElement = document.querySelector('.teacherFirstNameInput-js');
  this.lastNameElement = document.querySelector('.teacherLastNameInput-js');
  this.PersonConstructor = Teacher;

  this.localStorageKey = 'teachersCollection';
}

TeachersCollection.prototype = new BaseCollection();

const teachersCollection = new TeachersCollection();
teachersCollection.getCollection();

const addTeacherBtn = document.querySelector(".addTeacherBtn-js");
addTeacherBtn.addEventListener("click", teachersCollection.addPerson.bind(teachersCollection));