const addGroupBtn = document.querySelector('.addGroupBtn-js');

function Group (name, id) {
    this.name = name;
    this.id = id;
    this.element = null;
    this.list = groupList;
/*     this.remove = () => {
        this.element.remove();
        const selfIndex = groupList.findIndex((group) => group.id === this.id);

        groupList.splice(selfIndex, 1);
    } */

    this.render = function () {
        const list = document.querySelector('.groupList-js');
        const listItem = document.createElement('li');
        listItem.innerHTML = this.name;
        list.appendChild(listItem);
        this.element = listItem;
        
        const removeBtn = document.createElement('button');
        removeBtn.classList.add('removeBtn-js', 'btn', 'btn-primary');
        removeBtn.innerHTML = 'Delete Group';
        listItem.appendChild(removeBtn);

        removeBtn.addEventListener('click', this.remove.bind(this));
    }
}
const groupList = new Array;

addGroupBtn.addEventListener('click', function () {
    const groupName = document.querySelector('.groupNameInput-js').value;
    const groupId = groupList.length + 1;
    const group = new Group(groupName, groupId);
    groupList.push(group);
    group.render();
});

Group.prototype = new Entity();