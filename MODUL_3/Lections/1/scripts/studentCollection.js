const studentList = [];

function StudentCollection () {
    this.list = studentList;
    this.firstNameElement = document.querySelector('.studentFirstNameInput-js');
    this.lastNameElement = document.querySelector('.studentLastNameInput-js');
    this.PersonConstructor = Student;

    this.localStorageKey = 'studentsCollection';
}

StudentCollection.prototype = new BaseCollection();

const studentsCollection = new StudentCollection();
studentsCollection.getCollection();

const addStudentBtn = document.querySelector(".addStudentBtn-js");
addStudentBtn.addEventListener("click", studentsCollection.addPerson.bind(studentsCollection));