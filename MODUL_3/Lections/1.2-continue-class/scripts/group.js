const groupList = [];

function Group (name, id) {
    this.name = name;
    this.element = null;
    this.id = id;
    this.list = groupList;

    this.render = function () {
        const list = document.querySelector('.groupList-js');
        const listItem = document.createElement('li');
        listItem.innerHTML = this.name;
        list.appendChild(listItem);
        this.element = listItem;
        
        const removeBtn = document.createElement('button');
        removeBtn.classList.add('removeBtn-js', 'btn', 'btn-primary');
        removeBtn.innerHTML = 'Delete Group';
        listItem.appendChild(removeBtn);
        
        removeBtn.addEventListener('click', this.remove.bind(this));
    }
}
Group.prototype = new Entity();

const addGroupBtn = document.querySelector('.addGroupBtn-js');
addGroupBtn.addEventListener('click', function () {
    const groupId = groupList.length + 1;
    const groupName = document.querySelector('.groupNameInput-js').value;
    const group = new Group(groupName, groupId);
    groupList.push(group);
    group.render();
});
