class Teacher extends Entity {
    constructor(firstName, lastName, id) {          // то что должно быть у Teacher + то что ждет Entity
        super();                                    // только! то что ждет Entity
        this.firstName = firstName;
        this.lastName = lastName;
        this.element = null;
        this.id = id;
        this.listElement = document.querySelector(".teacherList-js");
        this.collection = teachersCollection;
    }
}