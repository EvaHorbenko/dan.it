const studentList = [];

const addStudentBtn = document.querySelector(".addStudentBtn-js");

const tooManyClickPromise = new Promise((resolve, reject) => {
  let clicks = 0;
  addStudentBtn.addEventListener("click", () => {
    const studentFirstName = document.querySelector('.studentFirstNameInput-js').value;
    const studentLastName = document.querySelector('.studentLastNameInput-js').value;

    if (studentFirstName === '' || studentLastName === '') {
      clicks++;
      if (clicks >= 5) {
        alert('Will resolve too many clicks')
        resolve()
      }
    }
  });

})

tooManyClickPromise.then(() => {
  alert('Too many clicks, sorry');
});

function StudentsCollection() {
  let studentSavedPromise;
  this.list = studentList;

  tooManyClickPromise.then(() => {
    studentSavedPromise.then(() => {
      console.log('Too many click resolved');
      studentSavedPromise.then(() => {
        console.log('Saved resolved');
        console.log(studentList);
        const studentsToRemove = studentList.splice(studentList.length - 4, studentList.length + 1)
        console.log(studentsToRemove);
      })
    })
  })
  this.addStudent = function () {
    const studentFirstName = document.querySelector('.studentFirstNameInput-js').value;
    const studentLastName = document.querySelector('.studentLastNameInput-js').value;

    const body = JSON.stringify({
      firstName: studentFirstName,
      lastName: studentLastName
    });

    studentSavedPromise = fetchJSON('http://localhost:3000/student', {
        method: 'POST',
        body,
        headers: {
          'Content-type': 'application/json'
        }
      }).then((response) => {
        const newStudent = response;
        const student = new Student(studentFirstName, studentLastName, newStudent.id);
        this.list.push(student);
        student.render();
      })      
  }

  this.syncCollection = function () {
    const studentListToSave = studentList.map((student) => {
      const firstName = student.firstName;
      const lastName = student.lastName;
      const id = student.id;

      return { firstName, lastName, id };
    });

    localStorage.setItem('studentsCollection', JSON.stringify({ 
      studentList: studentListToSave
    }));
  }

  this.removeById = function (id) {
    fetchJSON(`http://localhost:3000/student/${id}`, {
      method: 'DELETE'
    })
      .then(() => {
        const studentToRemove = studentList.find((student) => {
          return student.id === id;
        })
        studentToRemove.element.remove();

        const selfIndex = studentList.findIndex((person) => {
          return person.id === id;
        });
        studentList.splice(selfIndex, 1);
      })
  }

  this.getCollection = function () {
    fetchJSON('http://localhost:3000/student')
      .then((response) => {
        const collectionFromServer = response;
        collectionFromServer.forEach(element => {
          const firstName = element.firstName;
          const lastName = element.lastName;
          const id = element.id;
          const person = new Student(firstName, lastName, id);
          this.list.push(person);
          person.render();
        });
      });
  }
}

const studentsCollection = new StudentsCollection();
studentsCollection.getCollection();

addStudentBtn.addEventListener("click", studentsCollection.addStudent.bind(studentsCollection));