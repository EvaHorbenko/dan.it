const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')

const teacher1 = {
    id: 1,
    firstName: 'Severus',
    lastName: 'Snape'
}
const teacher2 = {
    id: 2,
    firstName: 'Minerva',
    lastName: 'MacGonnagal'
}
const teachers = [teacher1, teacher2];

const student1 = {
    id: 1,
    firstName: 'Harry',
    lastName: 'Potter'
}
const student2 = {
    id: 2,
    firstName: 'Draco',
    lastName: 'Malfoy'
}
const students = [student1, student2];

const app = express();
app.use(cors());
app.use(bodyParser.json());


app.delete('/teacher/:id', function (req, res) {
    const id = req.params.id;

    const index = teachers.findIndex((teacher) => {
        return teacher.id === Number(id);
    });
    const teacherToRemove = teachers[index]
    teachers.splice(index, 1);
    
    setTimeout(() => {
        res.send(teacherToRemove)
    }, 3000);
})

app.post('/teacher', function (req, res) {
    const newTeacher = req.body;
    const personId = teachers.length + 1;
  
    const teacherWithId = { 
      ...newTeacher,
      id: personId 
    };
  
    teachers.push(teacherWithId);
    res.send(teacherWithId);
});

app.get('/teacher', function (req, res) {
    res.send(teachers)
})


app.delete('/student/:id', function (req, res) {
    const id = req.params.id;

    const index = students.findIndex((student) => {
        return student.id === Number(id);
    });
    const studentToRemove = students[index]
    students.splice(index, 1);

    setTimeout(() => {
        res.send(studentToRemove);
    }, 3000);

})

app.post('/student', function (req, res) {
    const newStudent = req.body;
    const personId = students.length + 1;
  
    const studentWithId = { 
      ...newStudent,
      id: personId 
    };
  
    students.push(studentWithId);
    res.send(studentWithId);
});

app.get('/student', function (req, res) {
    res.send(students)
})


app.listen(3000, function () {
    console.log('Listening on 3000')
})
