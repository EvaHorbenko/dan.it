class Entity {
    render () {
        const listItem = document.createElement('li');
        listItem.innerHTML = this.firstName + ' ' + this.lastName;
        this.listElement.appendChild(listItem);
        this.element = listItem;

        const removeBtn = document.createElement('button');
        removeBtn.classList.add('removeBtn-js', 'btn', 'btn-primary');
        removeBtn.innerHTML = 'Delete';
        listItem.appendChild(removeBtn);
        
        removeBtn.addEventListener('click', this.remove.bind(this));
    }

    remove () {
        console.log('will wait');
        wait(1000).then(() => {
            this.collection.removeById(this.id)
            console.log('will remove');
        })
        // this.element.remove();
    }
}