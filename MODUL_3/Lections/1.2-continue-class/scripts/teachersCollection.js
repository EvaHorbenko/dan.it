const teacherList = [];

class TeachersCollection extends BaseCollection {
  constructor () {
    super ();
    this.list = teacherList;
    this.firstNameElement = document.querySelector('.teacherFirstNameInput-js');
    this.lastNameElement = document.querySelector('.teacherLastNameInput-js');
    this.PersonConstructor = Teacher;
    this.localStorageKey = 'teachersCollection';
  }

  addTeacher () {
    const teacherFirstName = document.querySelector('.teacherFirstNameInput-js').value;
    const teacherLastName = document.querySelector('.teacherLastNameInput-js').value;

    const body = JSON.stringify({
      firstName: teacherFirstName,
      lastName: teacherLastName
    });

    fetchJSON('http://localhost:3000/teacher', {
      method: 'POST',
      body,
      headers: {
        'Content-type': 'application/json'
      }
    }).then((response) => {
        const newTeacher = response;
        const teacher = new Teacher(teacherFirstName, teacherLastName, newTeacher.id);
        this.list.push(teacher);
        teacher.render();
      })
  }

  removeById(id) {
    fetchJSON(`http://localhost:3000/teacher/${id}`, {
      method: 'DELETE'
    }).then(() => {
        const teacherToRemove = teacherList.find((teacher) => {
          return teacher.id === id;
        })
        teacherToRemove.element.remove();
       
        const teacherIndex = teacherList.findIndex((person) => {
          return person.id === id;
        });
        teacherList.splice(teacherIndex, 1);
      })
  }

  getCollection () {
    fetchJSON('http://localhost:3000/teacher')
      .then((data) => {
        const collectionFromServer = data;
        collectionFromServer.forEach(element => {
          const firstName = element.firstName;
          const lastName = element.lastName;
          const id = element.id;
          const person = new Teacher(firstName, lastName, id);
          this.list.push(person);
          person.render();
        });
      })
  }

  filterTeachers (event) {
    const filteredList = teacherList.filter((teacher) => {
      return ((teacher.firstName + ' ' + teacher.lastName) || (teacher.lastName + ' ' + teacher.firstName)).includes(event.target.value)
    })
    const listElement = document.querySelector(".teacherList-js");
    listElement.innerHTML = ' ';

    filteredList.forEach(teacher => teacher.render());
  }
}

const teachersCollection = new TeachersCollection();
teachersCollection.getCollection();

const addTeacherBtn = document.querySelector(".addTeacherBtn-js");
addTeacherBtn.addEventListener("click", teachersCollection.addTeacher.bind(teachersCollection));

const searchInput = document.querySelector('.teacherSearchInput-js');
searchInput.addEventListener('input', teachersCollection.filterTeachers);