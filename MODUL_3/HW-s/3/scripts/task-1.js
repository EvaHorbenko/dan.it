/*      Теоретический вопрос:
            Обьясните своими словами, как вы понимаете, что такое деструктуризация и зачем она нужна

            деструктуризация позволяет разбивать сложные структуры (объекты, массивы, вложенные структуры) на более простые для работы непосредственно с ними
            способ назначения свойтсв объекта или значений массивов в переменные
*/

            /*      Задание 1        */
    /*      кажется тут не использована деструктуризация, но у меня нет идей как ее сюда применить        */
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

/* const unification = new Array;

function arrsUnification () {
    for (let arrNum = 0; arrNum < arguments.length; arrNum++) {
        arguments[arrNum].forEach(client => {
            if (!unification.includes(client)) {
                unification.push(client)
            }
        });
    }
}

arrsUnification(clients1, clients2)
console.log(unification);           */

const unification = [...clients1, ...clients2]    // Destruction
const cleenedUnification = [...new Set(unification)]
console.log(cleenedUnification);
