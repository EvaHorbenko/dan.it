      /*      Задание 3        */

const user1 = {
  name: "John",
  years: 30,
  // isAdmin: true
};

let {name, years: age, isAdmin = false} = user1;    // Destruction

console.log(`name: ${name}, age: ${age} years old, isAdmin: ${isAdmin}`);