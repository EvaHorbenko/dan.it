/*      Задание 7        */

const array = ['value', () => 'showValue'];

let [value, showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'
