            /*      Задание 2        */

const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
];

/* const charactersShortInfo = new Array;
for (let {name, lastName, age} of characters) {    // Destruction
    const charactersShortObj = {name, lastName, age};
    charactersShortInfo.push(charactersShortObj);
}
console.log(charactersShortInfo); */

const charactersShortInfo = characters.map(element => {
  let {gender, status, ...rest} = element;
  return rest;
});

console.log(charactersShortInfo);