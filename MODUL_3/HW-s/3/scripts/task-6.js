/*      Задание 6        */

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

/* let {name, surname, age = 44, salary = 250} = employee;    // Destruction

const extendedEmployee = {name, surname, age, salary}

console.log(employee);                // length: 2
console.log(extendedEmployee);        // length: 4 */

const newEmployee = {...employee, age: 44, salary: 222};
console.log(newEmployee);