/*      Задание 5        */

const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}


/* const [...newBooksArray] = books;    // Destruction
newBooksArray.push(bookToAdd)

console.log(books);               // length: 3
console.log(newBooksArray);       // length: 4 */

const newBook = [...books, bookToAdd];

console.log(newBook);
