/*      Теоретический вопрос:
            Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.

            AJAX обеспечивает фоновый обмен данными между браузером и сервером. Таким образом при обновлении данных страница не перезагружается полностью. Это ускоряет работу приложения
*/

const episodes = document.querySelector('.episodes');

function getFilmList(adress) {
    fetch(adress)
        .then((response) => {
            return response.json();
        })
        .then((films) => {
            films.forEach(film => {
                const id = film.episodeId
                renderMovie(film, id)
                getCharactersList(film.characters)

                .then(characterList => {
                    renderCharacters(characterList, id)
                })
            });
        });
}

function renderMovie (film, id) {
    let episodeLi = episodes.appendChild(document.createElement('li'));
    episodeLi.classList.add('episode');
    episodeLi.dataset.index = id;
    episodeLi.innerHTML = `<strong>Episode ${id}: ${film.name}</strong> <br> ${film.openingCrawl}`;
}

function getCharactersList(adress) {

    return new Promise((resolve, reject) => {
        const characters = [];
        adress.forEach(link => {
            fetch(link)
            .then(response => response.json())
            .then(character => {
                characters.push(character.name)
                if (characters.length === adress.length) {
                    resolve(characters);
                }
            })
            
        }) 
    })
}

function renderCharacters (characterList, id) {
    const episodeLiCollection = document.querySelectorAll('.episode');
    episodeLiCollection.forEach(episodeLi => {        
        if (episodeLi.dataset.index == id) {
            let characterWrapper = episodeLi.appendChild(document.createElement('ul'))

            characterList.forEach(eachCharacter => {
                let characterLi = characterWrapper.appendChild(document.createElement('li'))
                characterLi.innerHTML = eachCharacter;
            });
        }
    });
}

getFilmList('https://ajax.test-danit.com/api/swapi/films')