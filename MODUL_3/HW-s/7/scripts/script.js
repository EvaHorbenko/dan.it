class Card {
    constructor() {
        this.usersList = []
        this.postsList = []
        this.usersWithPostsList = []
        this.mainBlock = document.querySelector('.main-block')
    }

    get singleArray () {
        return this.getUserWithPosts()
    }

    async getUserWithPosts () {
        await this.getUsersArray()
        await this.getPostsArray()

        this.usersList.forEach(user => {
            const usersPosts = []
            this.postsList.forEach(post => {
                if (post.userId === user.id) {
                    usersPosts.push(post)
                }
            })
            const userWithPosts = { ...user, usersPosts: usersPosts}
            this.usersWithPostsList.push(userWithPosts)
        });
        this.getValuesToRender()
    }
    
    getUsersArray () {
        return fetch('https://ajax.test-danit.com/api/json/users')
            .then(response => response.json())
            .then(json => this.usersList = json)
    }

    getPostsArray () {
        return fetch('https://ajax.test-danit.com/api/json/posts')
            .then(response => response.json())
            .then(json => this.postsList = json)
    }

    getValuesToRender () {
        this.usersWithPostsList.forEach(user => {
            const userName = user.name
            const userEmail = user.email
            user.usersPosts.forEach(post => {
                const postTitle = post.title
                const postBody = post.body
                const postId = post.id
                this.renderPost(userName, userEmail, postTitle, postBody, postId)
            })
        });

        this.mainBlock.addEventListener('click', this.deleteCard.bind(this))
    }

    renderPost (userName, userEmail, postTitle, postInner, postId) {
        const sectionToRender = document.createElement('section');
        sectionToRender.dataset.id = postId
        sectionToRender.classList.add('post-wrapper')
        sectionToRender.innerHTML = `
                <div class="user">
                    <span class="user-name">${userName}</span>
                    <span class="user-email">${userEmail}</span>
                    <span class="delete-icon"><i class="fas fa-quidditch"></i></span>
                </div>
                <div class="post">
                    <h2 class="post-title">${postTitle}</h2>
                    <p class="post-inner">${postInner}</p>
                </div>`
        this.mainBlock.appendChild(sectionToRender)
    }

    async deleteCard (event) {
        let target = event.target
        const postId = Number(target.closest('.post-wrapper').dataset.id);
        
        const deleted = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
            method: 'DELETE'
        })

        if (deleted.ok) {
            this.usersWithPostsList.forEach(user => {
                return user.usersPosts.forEach(post => {
                    if (post.id === postId) {
                        user.usersPosts.splice(postId - 1, 1)
                    } 
                })
            })
    
            if (target.closest('.delete-icon')) {
                target.closest('.post-wrapper').remove()
            }
        }
    }
}

const card = new Card();
card.singleArray;
