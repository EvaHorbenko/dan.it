/*      Теоретический вопрос:
            Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.
        
            В случае когда возможна ошибка не в коде, а во входных данных, как например не валидные данные 
            с сервера или от пользователя (неправильный массив / JSON, некорректный ввод данных и тп)
*/

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const booksToShow = [];
const booksListWrapper = document.querySelector('#root');
const booksList = booksListWrapper.appendChild(document.createElement('ul'));

for (let i = 0; i < books.length; i++) {
    try {
        if (!books[i].author) {
            throw new SyntaxError('Current book has no author property')
        } else if (!books[i].name) {
            throw new SyntaxError('Current book has no name property')
        } else if (!books[i].price) {
            throw new SyntaxError('Current book has no price property')
        } else {
            booksToShow.push(books[i]);
        }
    } catch (errorReason) {
        console.log(errorReason)
    }
}

/* booksToShow.forEach(element => {
    let li = booksList.appendChild(document.createElement('li'));
    li.innerHTML = `${Object.keys(element)[0]}: ${Object.values(element)[0]}; <br>
                    ${Object.keys(element)[1]}: ${Object.values(element)[1]}; <br>
                    ${Object.keys(element)[2]}: ${Object.values(element)[2]}; `
}); */

for (let i = 0; i < booksToShow.length; i++) {
    let li = booksList.appendChild(document.createElement('li'));
    for (let propName in booksToShow[i]) {
        li.innerHTML += `${propName}: ${booksToShow[i][propName]}, <br>`
    }
}