/*      Теоретический вопрос:
            Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
        
        создается обьект-протопит с набором свойств (и/или значений). Другие обьекты могут наследовать свойства обьекта-протопита, если в самом обьекте они не указаны/перезаписаны. 
*/

class Employee {
    constructor (name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name () {
        return this._name
    }
    set name (newName) {
        if (newName.length >= 3 && isNaN(newName)) {
            this._name = newName.slice(0, 1).toUpperCase() + newName.slice(1).toLowerCase();
        }
    }

    get age () {
        return this._age
    }
    set age (newAge) {
        if (!isNaN(newAge) && newAge > 18) {
            this._age = newAge;
        }
    }

    get salary () {
        return this._salary
    }
    set salary (newSalary) {
        if (!isNaN(newSalary) && newSalary > 100) {
            this._salary = newSalary
        }
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }

    get salary () {
        return this._salary * 3
    }
    set salary (newSalary) {
        this._salary = newSalary
    }
}

let programmerJohn = new Programmer('john', 23, 250, 'js');
let programmerBob = new Programmer('bob', 44, 530, 'C#');
let programmerSam = new Programmer('sam', 19, 490, 'Php');

console.log(programmerJohn, programmerBob, programmerSam);