/*      Технический вопрос: асинхронность в Javascript
            Есть синхронные операции, и асинхронные. 
            Синхронные выполняются последовательно, одна за другой - в том порядке в котором написаны. 
            Асинхронные требуют времени, и чтоб не задерживать выполнение синхронного кода выполняются как бы "на фоне". Начинают выполнение когда до них доходит очередь, но окончить могут после того как выполнились следующие синхронные операции. Соответственно ответ то них приходит слишком поздно. Если результат асинхронной операции нужно записать в переменую, вероятно в переменную попадет Promise (обещание результата), а не сам результат, который мы ожидаем.
            await перед операцией дает понять что нужно подождать окончания выполнения, а уже потом выполнять код дальше. Таким образом асинхронные операции как будто становятся синхронными. Фактически они остаются асинхронными, просто окружающие их ждут. */

const ipApiFounderBtn = document.querySelector('.ip-api-founder-btn')

async function getIpApi() {
    let response = await fetch('https://api.ipify.org/?format=json')
    let ipObj = await response.json()
    let ip = ipObj.ip
    getJsonByIp(ip)
}

async function getJsonByIp(ip) {
    let response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`)
    let responseObj = await response.json()    
    renderIpResponse(responseObj)
}

function renderIpResponse(responseObj) {
    const responseRender = document.querySelector('.response')
    responseRender.innerHTML = `<li>Сontinent: ${responseObj.continent}</li>
                                <li>Country: ${responseObj.country}</li>
                                <li>Region: ${responseObj.regionName}</li>
                                <li>City: ${responseObj.city}</li>
                                <li>District: ${responseObj.district}</li>`
}

ipApiFounderBtn.addEventListener('click', getIpApi)