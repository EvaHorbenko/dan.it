const gulp = require('gulp');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify')
const sourcemaps = require('gulp-sourcemaps');

const converter = function () {
    return gulp.src('script.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist'))
};

gulp.task('watch', function () {
    gulp.watch('script.js', converter);
})