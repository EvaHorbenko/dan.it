// @ts-nocheck
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

function compileSass () {
  return gulp.src('./src/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
};

gulp.task('compile-sass', compileSass);

gulp.task('compile-sass-watch', function () {
    compileSass();
  gulp.watch('./src/**/*.scss', compileSass)
});