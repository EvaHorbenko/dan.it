const menuBtn = document.querySelector(".header__nav__menu-js");
const menuBlock = document.querySelector(".header__nav__items-wrapper-js");

const showMenuBlock = function () {
    menuBlock.classList.toggle('header__nav__items-wrapper--hidden')
    if (menuBlock.classList.contains('header__nav__items-wrapper--hidden')) {
        menuBtn.innerHTML = '<i class="fas fa-bars"></i>'
    } else {
        menuBtn.innerHTML = '<i class="fas fa-times"></i>'
    }
}

menuBtn.addEventListener('click', showMenuBlock)